# ocp-cicd-tool

A UBI8 minimal base image with the following packages:

1. OpenShift Client (aka `oc`)
2. Helm cli
3. jq cli

This image is specifically for providing CI/CD runtime tooling.

* It can be used within gitlab pipelines to access OpenShift and install Helm charts.

## Build container image

```bash
TAG=v1.1
IMAGE_NAME=ocp-cicd-tool
HELM_VERSION=3.7.1
OC_VERSION=4.10.11
JQ_VERSION=jq-1.6

# Build image with podman
podman build --no-cache -f ./Dockerfile -t ${IMAGE_NAME}:${TAG} \
  --build-arg=OC_VERSION=${OC_VERSION} \
  --build-arg=HELM_VERSION=${HELM_VERSION} \
  --build-arg=JQ_VERSION=${JQ_VERSION} .
  
# Verify packages
podman run --rm -it ${IMAGE_NAME}:${TAG} \
  echo "helm: $(helm version --short)" && \
  echo "oc: $(oc version --client -o yaml)" && \
  echo "jq: $(jq --version)"
```