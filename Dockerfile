FROM registry.access.redhat.com/ubi8-minimal:latest

LABEL maintainer="corbs"
LABEL name="ocp-cicd-tool"
LABEL version="1.0.0"
LABEL summary="UBI8 minimal with oc, helm and jq tools."
LABEL description="UBI8 minimal base image with client tooling to support CI/CD automation."
LABEL io.k8s.display-name="UBI8 minimal with oc, helm and jq tools."

ARG OC_VERSION="4.10.14"
ARG HELM_VERSION="3.7.1"
ARG JQ_VERSION="jq-1.6"

RUN \
  microdnf update && \
  microdnf install which gzip tar git make && \
  curl -OL \
    https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/${OC_VERSION}/openshift-client-linux.tar.gz && \
  tar -xvf openshift-client-linux.tar.gz -C /usr/local/bin/ && \
  chmod 755 /usr/local/bin/oc && \
  curl -OL \
    https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/helm/${HELM_VERSION}/helm-linux-amd64.tar.gz && \
  tar -xvf helm-linux-amd64.tar.gz -C /usr/local/bin/ && \
  chmod 755 /usr/local/bin/helm && \
  curl -OL https://github.com/stedolan/jq/releases/download/${JQ_VERSION}/jq-linux64 && \
  mv jq-linux64 /usr/local/bin/jq && \
  chmod 755 /usr/local/bin/jq && \
  microdnf remove tar && \
  microdnf clean all && \
  mkdir .kube && \
  chmod -R g+rw .kube && \
  rm -rf openshift-client-linux.tar.gz helm-linux-amd64.tar.gz
